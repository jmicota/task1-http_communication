#define _GNU_SOURCE 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include "dictionary.h"
#include "file_functionalities.h"

#define DEFAULT_PORT_NR   8080
#define LINE_SIZE         200
#define LETHAL_FAILURE    666

int main (int argc, char *argv[]) {
  int ret = 0, sock = 0, msgsock = 0, port_nr = DEFAULT_PORT_NR;
  bool GF = 0, HF = 0, HD_CCF = 0, HD_CLF = 0, HD_SRV = 0; // mode detecting flags
  char *filename = NULL, *filepath = NULL, *filepath_full = NULL, *real_filepath = NULL;
  char* msg = NULL, *tmp = NULL;
  FILE *fp = NULL;
  socklen_t len;
  struct sockaddr_in server;
  char line[LINE_SIZE + 1] = {0};
  const char DELIM[2] = "\n";

  // PROCESS ARGUMENTS
  if (argc < 3 || 4 < argc)
    exit(EXIT_FAILURE);

  if (argc > 3) {
    port_nr = atoi(argv[3]); // optional port number
    if (port_nr == 0)
      exit(EXIT_FAILURE);
  }

  // SET UP SERVER ENVIRONMENT
  char* srv_cat_path = malloc((MY_PATH_MAX + 1) * sizeof(char));
  char* correlated_path = malloc((MY_PATH_MAX + 1) * sizeof(char));
  if (srv_cat_path == NULL) 
    exit(EXIT_FAILURE);
  if (correlated_path == NULL) {
    free(srv_cat_path);
    exit(EXIT_FAILURE);
  }
  realpath(argv[1], srv_cat_path);
  realpath(argv[2], correlated_path);

  DIR *srv_dir = opendir(srv_cat_path); // find and check access to server directory
  if (srv_dir == NULL) {
    free(srv_cat_path);
    free(correlated_path);
    exit(EXIT_FAILURE);
  }
  closedir(srv_dir);

  FILE *corr_file = fopen(correlated_path, "r"); // open correlated servers file
  if (corr_file == NULL)
  if (srv_dir == NULL) {
    free(srv_cat_path);
    free(correlated_path);
    exit(EXIT_FAILURE);
  }
  free(correlated_path);
  

  // SET UP SERVER SOCKET
  sock = socket(PF_INET, SOCK_STREAM, 0);
  if (sock == -1) {
    free(srv_cat_path);
    exit(EXIT_FAILURE);
  }

  server.sin_family = AF_INET; // IPv4
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(port_nr);
  if (bind(sock, (struct sockaddr *)&server, (socklen_t)sizeof(server)) < 0) {
    free(srv_cat_path);
    exit(EXIT_FAILURE);
  }

  len = (socklen_t)sizeof(server);
  if (getsockname(sock, (struct sockaddr *)&server, &len) < 0) {
    free(srv_cat_path);
    exit(EXIT_FAILURE);
  }

  printf("Listening at port %d\n", ntohs(server.sin_port));
  if (listen(sock, 5) < 0) {
    free(srv_cat_path);
    exit(EXIT_FAILURE);
  }
  

  // ACCEPTING CLIENTS
  for (;;) {
    msgsock = accept(sock, (struct sockaddr *)NULL, NULL);
    if (msgsock == -1) {
      free(srv_cat_path);
      exit(EXIT_FAILURE);
    }
    
    memset(line, 0, LINE_SIZE); // reset memory for new client
    bool startline_flag = false, eol_carriage_ret = false, 
         saved_line = false, message_over_flag = false, file_found = false;
    HF = false; GF = false; HD_CCF = false; HD_CLF = false, HD_SRV = false; // reset flags
    size_t filesize = 0;
    char* my_slash = "/";
    char *line_part = NULL;

    while ((ret = recv(msgsock, line, LINE_SIZE, 0)) != 0) {

      if (ret <= 0) {
        free(srv_cat_path);
        exit(EXIT_FAILURE);
      }
      if ((ret = handle_delim_issues(line, &eol_carriage_ret)) != 0)
        break;

      char* token = strtok(line, DELIM); // get first part of read line (to '\n')
      if (token == NULL)
        token = line;
      else if (token == NULL && line_part != NULL) { // saved part of message doesnt have continuation
        saved_line = true;
        if (line_part != NULL)
          free(line_part);
        ret = ERR_FORMAT;
        break;
      }
      else if (line_part != NULL) { // connect saved message and token (into token)
        char* connected = connect_str(line_part, token);
        token = (char*) connected;
        saved_line = false;
        if (line_part != NULL)
          free(line_part);
      }
     
      while (token != NULL) {
        message_over_flag = false;

        // PROCESS POTENTIAL HTTP LINE
        if (eol_is_carriage_return(token)) {
          // START-LINE
          if (!startline_flag) { 
            ret = match_method(token, &GF, &HF);
            if (ret != 0)
              break;

            filepath = filepath_from_token(token);
            bool slash = single_slash(filepath);
            if ((ret = check_malloc(filepath, LETHAL_FAILURE)) != 0)
              break;
            filepath_full = connect_str(srv_cat_path, filepath);
            if ((ret = check_malloc(filepath_full, LETHAL_FAILURE)) != 0)
              break;

            DIR *dp = opendir(filepath_full); // request may be a directory
            if (dp != NULL && !slash) {
              ret = ERR_NFOUND;
              closedir(dp);
              break;
            }
            fp = fopen(filepath_full, "r");

            if (slash) {
              filename = malloc(2 * sizeof(char));
              strcpy(filename, my_slash);
            }
            else if (fp == NULL) { // FILE NOT ACCESSIBLE LOCALLY
              tmp = basename(filepath_full); // save filename
              if (tmp != NULL) {
                filename = malloc((strlen(tmp) + 1) * sizeof(char));
                if ((ret = check_malloc(filename, ERR_SRVSIDE)) != 0)
                  break;
                strcpy(filename, tmp);
              }
            }
            else { // FILE FOUND AND OPENED
              real_filepath = malloc((MY_PATH_MAX + 1) * sizeof(char));
              if ((ret = check_malloc(real_filepath, LETHAL_FAILURE)) != 0)
                break;
              realpath(filepath_full, real_filepath);
              bool inside_working_dir = is_prefix(srv_cat_path, real_filepath);
              if (!inside_working_dir) { 
                ret = ERR_NFOUND;
                break;
              }
              else { 
                filesize = get_filesize(fp);
                msg = build_prebody_message(filesize);
                if ((ret = check_malloc(msg, LETHAL_FAILURE)) != 0)
                  break;
                file_found = true;
              }
            }
            startline_flag = true;
          }
          // HEADER-LINE
          else {
            ret = match_header_field(token, &HD_CCF, &HD_CLF, &HD_SRV);
            
            // SENDING RESPONSE (correct message)
            if (ret == MESSAGE_OVER) {
              if (!file_found)
                msg = messsage_after_final_search(corr_file, filename);
              if ((ret = check_malloc(msg, LETHAL_FAILURE)) != 0)
                break;
              ret = send_msg(msg, msgsock);
              if (ret != 0)
                break;
              if (GF && (fp != NULL)) {
                if ((ret = send_file_contents(fp, msgsock, filesize)) != 0)
                  break;
              }
              if (HD_CCF) {
                ret = CLOSE_CONNECTION;
                break;
              }
              startline_flag = false;  // looking for start line again
              message_over_flag = true;
              free(msg);
              file_found = false;
            }
            else if (ret == ERR_FORMAT || ret == ERR_UNIMPL)
              break;
          }
        }
        // TOKEN WAS NOT A FULL HTTP LINE
        else { 
          if (line_part != NULL && token != NULL) {
            char* connected = connect_str(line_part, token); // save partial line
            if (connected == NULL) {
              ret = LETHAL_FAILURE;
              break;
            }
            line_part = connected;
            saved_line = true;
          }
          else {
            line_part = malloc((strlen(token) + 1) * sizeof(char));
            if (line_part == NULL) {
              ret = LETHAL_FAILURE;
              break;
            }
            strcpy(line_part, token);
            saved_line = true;
          }
        }
        // GET NEXT TOKEN
        token = strtok(NULL, DELIM);
      } 

      // HANDLING ERRORS FROM LINE PROCESSING
      if (ret == CLOSE_CONNECTION)
        break;    // disconnect
      if (saved_line || (!message_over_flag && startline_flag))
        ret = ERR_FORMAT;
      else if (ret == LETHAL_FAILURE) {
        ret = LETHAL_FAILURE;
        break;    // disconnect
      }
      if (ret != 0) {
        msg = build_err_message(ret);
        if ((ret = check_malloc(msg, LETHAL_FAILURE)) != 0)
          break;  // disconnect
        ret = send_msg(msg, msgsock);
        free(msg);
        break;    // disconnect
      }

      memset(line, 0, LINE_SIZE); // GET NEXT LINE
    } // end of while(recv)

    close(msgsock);
    if (fp != NULL)
      fclose(fp);
  } // end of for (;;)

  close(sock);
  fclose(corr_file);
  free(filepath);
  free(filepath_full);
  free(real_filepath);
  free(filename);
  return 0;
}
