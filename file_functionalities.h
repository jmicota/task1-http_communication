#define _GNU_SOURCE 

#ifndef FILEFUN_H
#define FILEFUN_H

/** If malloc failed, return given code. */
extern int check_malloc(char* ptr, int code);

/** Compute number of characters in file. */
extern size_t get_filesize(FILE* fp);

/** Handle split CRLF case and detect double delimiters. 
 * Return potential error code. */
extern int handle_delim_issues(char* line, bool* eol_carr_ret);

/** Return http message with info about changed location or not found error. */
extern char* messsage_after_final_search(FILE* fp, char* search);

/** Send message to client. Return potential error code. */
extern int send_msg(char* msg, int msgsocket);

/** Send file contents to client. Return potential error code. */
extern int send_file_contents(FILE* file, int msgsocket, size_t filesize);

#endif
