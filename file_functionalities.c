#define _GNU_SOURCE 

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include "dictionary.h"
#include "file_functionalities.h"


int check_malloc(char* ptr, int code) {
  if (ptr == NULL)
    return code;
  return 0;
}

size_t get_filesize(FILE* fp){
  fseek(fp, 0, SEEK_END);               // go to end of file
  size_t filesize = ftell(fp);          // get current file pointer
  fseek(fp, 0, SEEK_SET);               // go back to beginning of file
  return filesize;
}

int handle_delim_issues(char* line, bool* eol_carr_ret){
  if (search_double_endl(line))
    return ERR_FORMAT; // "\n\n" appears in line (\n is delim for strtok)
  if (*eol_carr_ret && !starts_with_endl(line))
    return ERR_FORMAT; // prev line ended with \r, \n not first in this line
  *eol_carr_ret = eol_is_carriage_return(line); // check if this line ends with '\r'
  return 0;
}

char* messsage_after_final_search(FILE* fp, char* search) {
  if (search == NULL || strlen(search) == 0)
    return build_err_message(ERR_NFOUND);
  
  char* delim = "\t";
  char buff[LINE_MAX];
  while (fgets(buff, sizeof(buff), fp) != NULL) {
    char* tmp = strtok(buff, delim);
    char* filename = basename(tmp);
    if (strcmp(filename, search) == 0 ||
       (strcmp(search, "/") == 0 && strcmp(tmp, "/") == 0)) { // if found
      char* ip = strtok(NULL, delim);    // get server ip
      char* port = strtok(NULL, delim);  // get port nr
      size_t port_len = strlen(port);
      port[port_len - 1] = '\0';         // removing newline character from port
      fseek(fp, 0, SEEK_SET);            // go back to beginning of file
      return build_location_message(buff, ip, port);
    }
  }
  fseek(fp, 0, SEEK_SET);                // go back to beginning of file
  return build_err_message(ERR_NFOUND);
}

int send_msg(char* msg, int msgsocket) {
  int ret = 0;
  if (send(msgsocket, msg, strlen(msg), MSG_NOSIGNAL) < 0)
    ret = ERR_SRVSIDE;
  // free(msg);
  return ret;
}

int send_file_contents(FILE* file, int msgsocket, size_t filesize) {
  if (filesize == 0)
    return 0;                           // if file empty, do nothing
  if (file == NULL)
    return ERR_SRVSIDE;

  char line[LINE_MAX];
  while(fgets(line, sizeof line, file)) {
    if (send(msgsocket, line, strlen(line), MSG_NOSIGNAL) < 0)
      return ERR_SRVSIDE;
  }
  return 0;
}
