#ifndef DICT_H
#define DICT_H

#define SUCCESS             200
#define ERR_FMOVED          302
#define ERR_FORMAT          400
#define ERR_NFOUND          404
#define ERR_SRVSIDE         500
#define ERR_UNIMPL          501
#define CLOSE_CONNECTION    333
#define MESSAGE_OVER        334

#define MY_PATH_MAX         8192

extern bool eol_is_carriage_return(char*);
extern bool starts_with_endl(char*);
extern bool search_double_endl(char*);
extern bool is_prefix(char*, char*);
extern bool single_slash(char*);

extern int match_method(char*, bool*, bool*);
extern int match_header_field(char*, bool*, bool*, bool*);

extern char* build_location_message(char*, char*, char*);
extern char* build_err_message(int);
extern char* build_prebody_message(size_t);

extern char* connect_str(char*, char*);
extern char* filepath_from_token(char*);

#endif
