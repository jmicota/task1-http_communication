all: serwer

dictionary.o: dictionary.cpp dictionary.h
	g++ -Wall -Wextra -O2 -std=c++17 dictionary.cpp -c

file_functionalities.o: file_functionalities.c file_functionalities.h
	gcc -Wall -Wextra -O2 -std=c11 file_functionalities.c -c

server.o: server.c
	gcc -Wall -Wextra -O2 -std=c11 server.c -c

serwer: dictionary.o file_functionalities.o server.o
	g++ -Wall -Wextra -O2 -std=c++17 dictionary.o file_functionalities.o server.o -o serwer

clean:
	rm dictionary.o file_functionalities.o server.o serwer